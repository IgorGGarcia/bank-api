from src.crud.base_crud import BaseCRUD
from src.database.models.customer_model import Customer


class CustomerCRUD(BaseCRUD):

    def __init__(self):
        super().__init__(Customer)
