from typing import Any

from sqlalchemy.orm import Session

from src.crud.base_crud import BaseCRUD
from src.database.models.account_model import Account


class AccountCRUD(BaseCRUD):

    def __init__(self):
        super().__init__(Account)

    def create(self, db: Session, data: Any):
        dict_data = dict(data)
        account = Account()
        account.customer_id = dict_data.get('customerID')
        account.balance = 0.0

        db.add(account)
        db.commit()
        db.refresh(account)
        return account

    def find_by_customer_id(self, db: Session, customer_id: str):
        account = db.query(Account).filter_by(customer_id=customer_id).first()
        return account
