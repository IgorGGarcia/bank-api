from src.crud.base_crud import BaseCRUD
from src.database.models.transactions_model import Transaction


class TransactionCRUD(BaseCRUD):

    def __init__(self):
        super(TransactionCRUD, self).__init__(Transaction)
