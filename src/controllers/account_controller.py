import logging

from fastapi import HTTPException
from sqlalchemy.orm import Session

from src.controllers.base_controller import BaseController
from src.controllers.transaction_controller import TransactionController
from src.crud.account_crud import AccountCRUD
from src.schemas.current_account_schema import (
    CurrentAccountCreate,
    CurrentAccountResponse
)


class AccountController(BaseController):

    def __init__(self):
        super(AccountController, self).__init__(AccountCRUD)

    def handle_create(self, db: Session, data: CurrentAccountCreate):
        data_dict = dict(data)
        customer_id = data_dict.get('customerID')
        account = AccountCRUD().find_by_customer_id(
            db,
            customer_id
        )

        if account is None:
            account = AccountCRUD().create(db, data)

        balance = float(data_dict.get('initialCredit'))
        if balance != 0:
            try:
                TransactionController().handle_realise_transfer(
                    db,
                    account.id,
                    balance,
                    commit=False
                )
                account.balance += balance
                db.commit()
            except HTTPException as err:
                logging.error(err.detail['error'])
                db.rollback()
                raise err

        return account

    def handle_get(self, db: Session, object_id: str):
        account = AccountCRUD().get(db, id=object_id)
        account_dict = CurrentAccountResponse(
            customerID=account.customer_id,
            id=account.id,
            balance=account.balance
        )
        return account_dict
