from sqlalchemy.orm import Session

from src.crud.customer_crud import CustomerCRUD
from src.controllers.base_controller import BaseController
from src.controllers.transaction_controller import TransactionController


class CustomerController(BaseController):

    def __init__(self):
        super(CustomerController, self).__init__(CustomerCRUD)

    def handle_get(self, db: Session, object_id: str):
        customer = super().handle_get(db, object_id)

        transactions = []
        balance = 0.0
        if not (customer.account is None):
            transactions = TransactionController().handle_list_by_account_id(
                db,
                customer.account.id
            )

            transactions = list(map(lambda t: t.__dict__, transactions))
            balance = customer.account.balance

        customer.transactions = transactions
        customer.balance = balance
        return customer
