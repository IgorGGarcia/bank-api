from fastapi import HTTPException
from sqlalchemy.orm import Session

from src.controllers.base_controller import BaseController
from src.crud.transaction_crud import TransactionCRUD
from src.database.models.transactions_model import (
    CategoryTransactionEnum,
    Transaction
)


class TransactionController(BaseController):

    def __init__(self):
        super(TransactionController, self).__init__(TransactionCRUD)

    def handle_realise_transfer(
        self,
        db: Session,
        account_id: str,
        value: float,
        commit=True
    ):
        if value < 0:
            raise HTTPException(status_code=400, detail={
                'error': """
                    InvalidValue: InitialCredit mayn't be a negative number
                """
            })

        return TransactionCRUD().create(db, {
            "account_id": account_id,
            "value": value,
            "type": CategoryTransactionEnum.TRANSFER
        }, commit)

    def handle_list_by_account_id(self, db: Session, account_id: str):
        records = db.query(Transaction).filter_by(account_id=account_id).all()
        return records
