from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from src.controllers.account_controller import AccountController
from src.database.config import get_db
from src.schemas.current_account_schema import (
    CurrentAccountCreate,
    CurrentAccountBase,
    CurrentAccountResponse
)


account_router = APIRouter(prefix='/account')


@account_router.post('/', response_model=CurrentAccountBase, status_code=201)
def handle_create_account(
    data: CurrentAccountCreate,
    db: Session = Depends(get_db)
):
    """
    This route is used for creating new current accounts.
    """
    return AccountController().handle_create(db, data)


@account_router.get('/{account_id}', response_model=CurrentAccountResponse)
def handle_get_account(account_id: str, db: Session = Depends(get_db)):
    return AccountController().handle_get(db, account_id)
