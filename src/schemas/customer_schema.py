from datetime import datetime
from uuid import UUID

from typing import Optional, List
from pydantic import BaseModel

from src.schemas.transaction_schema import TransactionByAccountResponse


class CustomerBase(BaseModel):
    id: UUID

    class Config:
        orm_mode = True


class CustomerCreate(BaseModel):
    name: str
    surname: str

    class Config:
        orm_mode = True


class CustomerListResponse(CustomerBase):
    name: str
    surname: str
    created_at: datetime
    updated_at: Optional[datetime]


class CustomerResponse(CustomerListResponse):
    balance: Optional[float] = 0.0
    transactions: List[TransactionByAccountResponse] = []
