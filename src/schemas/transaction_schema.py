from datetime import datetime
from uuid import UUID

from pydantic import BaseModel

from src.database.models.transactions_model import CategoryTransactionEnum


class TransactionByAccountResponse(BaseModel):
    id: UUID
    value: float
    type: CategoryTransactionEnum
    created_at: datetime

    class Config:
        orm_mode = True
