from uuid import UUID

from pydantic import BaseModel


class CurrentAccountBase(BaseModel):
    id: UUID

    class Config:
        orm_mode = True


class CurrentAccountCreate(BaseModel):
    customerID: str
    initialCredit: float = 0.0


class CurrentAccountResponse(CurrentAccountBase):
    customerID: UUID
    balance: float
