from datetime import datetime
from uuid import uuid4

from sqlalchemy import Column, Float, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from src.database.config import Base, GUID
from src.database.models.transactions_model import Transaction # noqa


class Account(Base):
    __tablename__ = "account"

    id = Column(
        GUID(),
        primary_key=True,
        index=True,
        unique=True,
        default=uuid4
    )
    balance = Column(Float, default=0.0)
    customer_id = Column(GUID(), ForeignKey("customer.id"))
    transactions = relationship(
        "Transaction",
        cascade="all, delete-orphan",
        uselist=True
    )
    created_at = Column(DateTime, default=datetime.now())
    updated_at = Column(DateTime, nullable=True)
