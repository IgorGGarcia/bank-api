from uuid import uuid4
from datetime import datetime

from sqlalchemy import Column, DateTime, String
from sqlalchemy.orm import relationship

from src.database.config import Base, GUID
from src.database.models.account_model import Account # noqa


class Customer(Base):
    __tablename__ = "customer"

    id = Column(
        GUID(),
        primary_key=True,
        index=True,
        default=uuid4,
        unique=True
    )
    name = Column(String, nullable=False)
    surname = Column(String, nullable=False)

    created_at = Column(DateTime, default=datetime.now())
    updated_at = Column(DateTime, nullable=True)
    account = relationship(
        "Account",
        cascade="all, delete-orphan",
        uselist=False
    )
