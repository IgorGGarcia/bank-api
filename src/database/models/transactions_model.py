import enum
from datetime import datetime
from uuid import uuid4

from sqlalchemy import Column, DateTime, Enum, Float, ForeignKey

from src.database.config import Base, GUID


class CategoryTransactionEnum(enum.Enum):
    DEPOSIT = 'deposit'
    WITHDRAW = 'withdraw'
    TRANSFER = 'transfer'


class Transaction(Base):
    __tablename__ = 'transaction'
    id = Column(
        GUID(),
        primary_key=True,
        unique=True,
        default=uuid4,
        index=True
    )
    account_id = Column(GUID(), ForeignKey('account.id'))
    value = Column(Float, nullable=False)
    type = Column(Enum(CategoryTransactionEnum), nullable=False)
    created_at = Column(DateTime, default=datetime.now())
