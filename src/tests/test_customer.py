from src.tests.test_database import client, get_first_object


def test_get_customers():
    response = client.get('/customer/')

    assert response.status_code == 200
    assert type(response.json()) == list


def test_create_customer():
    response = client.post('/customer/', None, {
        "name": "Customer",
        "surname": "Test"
    })

    assert response.status_code == 201
    assert len(response.json()) == 1
    assert 'id' in list(dict(response.json()).keys())


def test_get_a_customer():
    first = get_first_object(client, '/customer')

    response = client.get(f'/customer/{first["id"]}')
    keys_expected = [
        'id', 'created_at', 'updated_at', 'name',
        'surname', 'balance', 'transactions'
    ]
    response_keys = list(dict(response.json()).keys())

    result = list(filter(lambda x: not(x in keys_expected), response_keys))
    assert len(result) == 0


def test_patch_a_customer():
    customer = get_first_object(client, '/customer/')
    customer_id = customer["id"]
    response = client.patch(
        f'/customer/{customer_id}',
        '''{
            "name": "Test",
            "surname": "Update"
        }'''
    )
    assert response.status_code == 204

    response_updated = client.get(f'/customer/{customer_id}')
    assert response_updated.status_code == 200
    assert response_updated.json()['name'] == 'Test'
    assert response_updated.json()['surname'] == 'Update'


def test_delete_customer():
    customer = get_first_object(client, '/customer/')
    customer_id = customer['id']
    response = client.delete(f'/customer/{customer_id}')

    assert response.status_code == 204

    response_test = client.get(f'/customer/{customer_id}')
    assert response_test.status_code == 404
