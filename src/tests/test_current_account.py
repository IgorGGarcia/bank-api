from src.tests.test_database import client, get_first_object
from src.tests.test_customer import test_create_customer, test_delete_customer


def test_create_current_account():
    test_create_customer()
    customer = get_first_object(client, '/customer/')

    response = client.post("/account/", None, {
            "customerID": customer['id'],
            "initialCredit": 0
        }
    )

    assert response.status_code == 201
    assert 'id' in dict(response.json()).keys()
    test_delete_customer()
