from src.tests.test_database import client, get_first_object
from src.tests.test_customer import (
    test_create_customer,
    test_delete_customer
)


def test_post_a_transaction():
    test_create_customer()
    customer = get_first_object(client, '/customer/')

    response = client.post('/account/', None, {
        "customerID": f"{customer['id']}",
        "initialCredit": 100
    })

    assert response.status_code == 201
    test_delete_customer()
