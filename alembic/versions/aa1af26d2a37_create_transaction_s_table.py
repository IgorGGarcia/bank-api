"""create transaction's table

Revision ID: aa1af26d2a37
Revises: 7fc45112c53e
Create Date: 2022-06-11 00:37:29.142147

"""
from alembic import op
import sqlalchemy as sa

from src.database.config import GUID

# revision identifiers, used by Alembic.
revision = 'aa1af26d2a37'
down_revision = '7fc45112c53e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('transaction',
    sa.Column('id', GUID(), nullable=False),
    sa.Column('account_id', GUID(), nullable=True),
    sa.Column('value', sa.Float(), nullable=False),
    sa.Column('type', sa.Enum('DEPOSIT', 'WITHDRAW', 'TRANSFER', name='categorytransactionenum'), nullable=False),
    sa.ForeignKeyConstraint(['account_id'], ['account.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_transaction_id'), 'transaction', ['id'], unique=True)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_transaction_id'), table_name='transaction')
    op.drop_table('transaction')
    # ### end Alembic commands ###
