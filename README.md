# Bank API

## Summary

1. [Goals](#1-goals)
2. [Technologies and tools](#2-tecnologies-and-tools)
3. [Setup](#3-setup)
4. [Running application](#4-running-application)
5. [Features](#5-features)
6. [Documentation](#6-documentation)
7. [Tests](#7-tests)


### 1. Goals

The assessment consists of an API to open a new "current account" of already 
existing customers.

* The API will expose an endpoint that accepts the user information 
(customerID, initialCredit).
* Once the endpoint is called, a new account will be opened and connected to 
the user whose ID is customerID.
* Also, if initialCredit is not 0, a transaction will be sent to the new 
account.
* Another Endpoint will output the user information showing Name, Surname, 
balance, and transactions of the accounts.


### 2. Tecnologies and tools

| # | Use case |
|--|--|
| Alembic    | To manager database migrations |
| Docker/Docker-compose | "Virtual machines"  |
| FastAPI    | Framework to API server        |
| Postgresql | Database                       |
| SQLAlchemy | ORM                            |
| Swagger    | To generate the documentation  |

### 3. Setup

> &#9888; If you are using docker/docker-compose, skip this step.

> &#9888; It's required Python >= 3.9

It's recommended that you create a virtual enviroment. To do this, 
you can use the [virtualenv](https://docs.python.org/pt-br/3/library/venv.html). 
In the root folder of the project, type the follow command (Just type once):
```
python3 -m venv venv
```

To active the enviromnent, type:

- On linux:
     ```
     source venv/bin/activate
     ```  
  
  On Windows:
     ```
     ./venv/Script/activate.bat
     ```

Now, just install the dependencies typing:
```
pip install -r requirements.txt
```

Finally, create a copy from file `.env.example` and rename the copy to `.env` 
and change the value of variable `POSTGRES_HOST` to `127.0.0.1`

### 4. Running application

> &#9888; To this step, you **need** execute the [Setup recomendations](#3-setup).

> &#9888; Make sure that you have a copy from file `.env.example` renamed to `.env`.

You can execute the application by three ways:

  1. **Terminal/Prompt**
    
      In the root folder, type:

     ```
     uvicorn main:app --host=127.0.0.1 --port 8000
     ```
  
  2. **Makefile**
      > &#9888; To this step, you need have the [make](http://gnuwin32.sourceforge.net/packages/make.htm) installed
      
      In the root folder, type:

     ```
     make init_dev_server
     ```
    
     1. **Docker/Docker-compose**
           > &#9888; To run it this way, you need install [Docker](https://docs.docker.com/get-docker/) and [Docker-compose](https://docs.docker.com/compose/)

           In the root folder, type:
     
           ```
           docker-compose up -d
           ```

           The API will be running on port [8000](http://localhost:8000) and 
        database will be running on port [5432](http://localhost:5432).
 
           When the containers start, at first time, you will need enter on 
        container `bank_api` and run the migrations. You can type the follow 
        commands in other terminal to do it.

           ```
           docker exec -it bank_api bash
           ```
        and after type to running the migrations:
           ```
           alembic upgrade head
           ```


### 5. Features

##### **Customer**
* &#9989; Get customer data
* &#9989; Create new customers
* &#9989; Delete a customer
* &#9989; Update a customer

##### **Accounts**
* &#9989; Create a new account
* &#9989; Get account details

> Note:
> The challenge asked just two routes, the outher routes are bonus:
>  * To recovery a user's data. In this api is the route [`/customer/{customerID}`](http://localhost:8000/customer/)
> * Receive a customerID and balance and create a new account. In this API is the endpoint [`/account/`](http://localhost:8000/account/) 

### 6. Documentation

> &#9888; To see the documentation, make sure the containers are running.

> &#10071; The endpoints requested by the challenge are:
> * GET `/customer/{customerID}`
> * POST `/account/` <br/>
> The other endpoints are bonus.

The routes and documentation will be avaliable on route 
[/docs](http://localhost:8000/docs) and [/redoc](http://localhost:8000/redoc).




### 7. Tests

To run the tests, you just need type:

```
pytest ./src/
```

or use the makefile
```
make tests
```

> &#9888; If you are using docker, you will need run the command inside 
> api's container, type:
> 
> `docker exec -it bank_api bash`